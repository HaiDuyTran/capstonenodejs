const sequelize = require("../models/index");
const { successCode, failCode, errorCode } = require("../ultis/reponse");
const init_models = require("../models/init-models");
const model = init_models(sequelize);

const getCinemaSystemInfor = async (req, res) => {
  try {
    let thongTinHeThongRap = await model.HeThongRap.findAll();
    successCode(res, thongTinHeThongRap, "Xử lý thành công");
  } catch (error) {
    errorCode(res, "Xử lý thất bại");
  }
};

const getCinemaClusterBySystem = async (req, res) => {
  try {
    let { ma_he_thong_rap } = req.query;
    let cumRapTheoHeThong = await model.CumRap.findAll({
      where: { ma_he_thong_rap },
    });
    successCode(res, cumRapTheoHeThong, "Thao tác thành công");
  } catch {
    errorCode(res, "Xử lý thất bại");
  }
};

//LayThongTinLichChieuHeThongRap
const getSheduleInforSystem = async (req, res) => {
  try {
    let { ma_he_thong_rap } = req.query;
    let data1 = await model.CumRap.findAll({
      include: ["RapPhims"],
      where: { ma_he_thong_rap },
    });
    data1child = data1[0].ma_cum_rap;
    let data2 = await model.RapPhim.findAll({
      include: ["ma_phim_Phims"],
      where: { ma_cum_rap: data1child },
    });
    successCode(res, data2, "Thao tác thành công");
  } catch {
    errorCode(res, "Xử lý thất bại");
  }
};

// LayThongTinLichChieuPhim
const getSheduleInforMovie = async (req, res) => {
  try {
    let { ma_phim } = req.query;
    let data = await model.Phim.findAll({
      include: ["ma_rap_RapPhims"],
      where: { ma_phim },
    });
    successCode(res, data, "success");
  } catch {}
};

module.exports = {
  getCinemaSystemInfor,
  getCinemaClusterBySystem,
  getSheduleInforSystem,
  getSheduleInforMovie,
};
