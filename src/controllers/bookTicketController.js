const sequelize = require("../models/index");
const { successCode, failCode, errorCode } = require("../ultis/reponse");
const init_models = require("../models/init-models");
const model = init_models(sequelize);

// Lay danh sach phong ve
const getTicketList = async (req, res) => {
  try {
    let { ma_lich_chieu } = req.params;
    let data = await model.LichChieu.findAll({
      include: ["ma_phim_Phim"],
      where: { ma_lich_chieu },
    });
    let datVe = await model.DatVe.findAll({
      include: ["ma_ghe_Ghe"],
      where: { ma_lich_chieu },
    });
    successCode(res, [{ data }, datVe], "Thao tác thành công");
  } catch {
    errorCode(res, "Lỗi backend");
  }
};

// dat ve
const postBookTicket = async (req, res) => {
  try {
    let { tai_khoan, ma_lich_chieu, ma_ghe } = req.body;
    let check = await model.NguoiDung.findAll({
      where: { tai_khoan: tai_khoan },
    });
    let check2 = await model.DatVe.findAll({
      where: { tai_khoan: tai_khoan, ma_lich_chieu: ma_lich_chieu },
    });
    if (check.length != 0) {
      let newTicket = {
        tai_khoan,
        ma_lich_chieu,
        ma_ghe,
      };
      if (check2.length != 0) {
        failCode(res, "", "Mã lịch chiếu bị trùng");
      } else {
        let result = await model.DatVe.create(newTicket);
        successCode(res, result, "Thêm thành công");
      }
    } else {
      failCode(res, "", "Tài khoản không tồn tại");
    }
  } catch (err) {
    errorCode(res, "Lỗi backend");
  }
};

// dat lich chieu
const postBookShedule = async (req, res) => {
  try {
    let { ma_rap, ma_phim, ngay_gio_chieu, gia_ve } = req.body;
    let newShedule = {
      ma_rap,
      ma_phim,
      ngay_gio_chieu,
      gia_ve,
    };
    let result = await model.LichChieu.create(newShedule);
    successCode(res, result, "Thêm thành công");
  } catch (err) {
    errorCode(res, "Lỗi backend");
  }
};

module.exports = { getTicketList, postBookTicket, postBookShedule };
