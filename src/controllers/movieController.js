const sequelize = require("../models/index");
const { successCode, failCode, errorCode } = require("../ultis/reponse");
const init_models = require("../models/init-models");
const model = init_models(sequelize);

// GET

// Lấy danh sách banner
const getBanner = async (req, res) => {
  try {
    let danhsachBanner = await model.Banner.findAll();
    successCode(res, danhsachBanner, "Xử lý thành công");
  } catch (err) {
    errorCode(res, "Xử lý thất bại");
  }
};

// Lấy danh sách phim
const getMovieList = async (req, res) => {
  try {
    let danhsachPhim = await model.Phim.findAll();
    successCode(res, danhsachPhim, "Xử lý thành công");
  } catch (err) {
    errorCode(res, "Xử lý thất bại");
  }
};

// Lấy danh sách phim phân trang
const getMovieListByPage = async (req, res) => {
  try {
    let { soPhanTuTrenTrang, soTrang } = req.query;

    let danhsachTrangPhim = await model.Phim.findAndCountAll({
      offset: +soTrang * +soPhanTuTrenTrang - +soPhanTuTrenTrang,
      limit: +soPhanTuTrenTrang,
    });
    successCode(res, danhsachTrangPhim, "Xử lý thành công");
  } catch (err) {
    errorCode(res, "Xử lý thất bại");
  }
};

// Lấy danh sách phim theo ngày
const getMovieListByDate = async (req, res) => {
  try {
    let { soPhanTuTrenTrang, soTrang, tuNgay, denNgay } = req.query;
    let danhsachTrangPhim = await model.Phim.findAndCountAll({
      offset: +soTrang * +soPhanTuTrenTrang - +soPhanTuTrenTrang,
      limit: +soPhanTuTrenTrang,
    });
    let newdatex = tuNgay.split("-").reverse().join("-");
    let newdatey = denNgay.split("-").reverse().join("-");
    const x = new Date(`${newdatex}`);
    const y = new Date(`${newdatey}`);
    let danhsachNgayPhim = [];
    danhsachTrangPhim.rows.forEach((element) => {
      let z = new Date(`${element.dataValues.ngay_khoi_chieu}`);
      if (z < y && z > x) {
        danhsachNgayPhim.push(element);
      }
    });
    successCode(res, danhsachNgayPhim, "Thao tác thành công");
  } catch (err) {
    errorCode(res, "Xử lý thất bại");
  }
};

// Lấy thông tin phim
const getMovieDetail = async (req, res) => {
  try {
    let { ma_phim } = req.query;
    let thongTinPhim = await model.Phim.findAll({
      where: { ma_phim: ma_phim },
    });
    successCode(res, thongTinPhim, "Xử lý thành công");
  } catch (err) {
    errorCode(res, "Xử lý thất bại");
  }
};

// upload hinh
const postUploadImage = (req, res) => {
  const fs = require("fs");
  fs.readFile(process.cwd() + "/" + req.file.path, (err, data) => {
    let fileName = `data:${req.file.mimetype};base64,${Buffer.from(
      data
    ).toString("base64")}`;
    fs.unlinkSync(process.cwd() + "/" + req.file.path);
    res.send(fileName);
  });
};

// Thêm phim - QuanLyPhim
const postNewMovie = async (req, res) => {
  try {
    let {
      ten_phim,
      trailer,
      hinh_anh,
      mota,
      ngay_khoi_chieu,
      danh_gia,
      hot,
      dang_chieu,
      sap_chieu,
    } = req.body;

    let movieNew = {
      ten_phim,
      trailer,
      hinh_anh,
      mota,
      ngay_khoi_chieu,
      danh_gia,
      hot,
      dang_chieu,
      sap_chieu,
    };
    let result = await model.Phim.create(movieNew);
    successCode(res, result, "Xử lý thành công");
  } catch (error) {
    errorCode(res, "Xử lý thất bại");
  }
};

// cap nhat phim
const putUpdateMovie = async (req, res) => {
  try {
    let { id } = req.params;
    let {
      ten_phim,
      trailer,
      hinh_anh,
      mota,
      ngay_khoi_chieu,
      danh_gia,
      hot,
      dang_chieu,
      sap_chieu,
    } = req.body;
    let checkMovie = await model.Phim.findByPk(id);
    if (checkMovie) {
      let movieUpdate = {
        ma_phim: id,
        ten_phim,
        trailer,
        hinh_anh,
        mota,
        ngay_khoi_chieu,
        danh_gia,
        hot,
        dang_chieu,
        sap_chieu,
      };
      await model.Phim.update(movieUpdate, {
        where: { ma_phim: id },
      });
      successCode(res, movieUpdate, "Cập nhật thành công !");
    } else {
      failCode(res, "", "Mã phim không tồn tại");
    }
  } catch (error) {
    errorCode(res, "Lỗi backend");
  }
};
// xoa phim
const deleteMovie = async (req, res) => {
  try {
    let { id } = req.params;
    let checkMovie = await model.Phim.findByPk(id);
    if (checkMovie) {
      await model.Phim.destroy({
        where: { ma_phim: id },
      });
      successCode(res, "", "Xóa phim thành công !");
    } else {
      failCode(res, "", "Mã phim không tồn tại");
    }
  } catch (error) {
    errorCode(res, "Lỗi backend");
  }
};

module.exports = {
  getBanner,
  getMovieList,
  getMovieListByPage,
  getMovieListByDate,
  getMovieDetail,
  postNewMovie,
  postUploadImage,
  putUpdateMovie,
  deleteMovie,
};
