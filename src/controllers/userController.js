const sequelize = require("../models/index");
const { successCode, failCode, errorCode } = require("../ultis/reponse");
const init_models = require("../models/init-models");
const model = init_models(sequelize);
const { encodeToken } = require("../middleware/auth");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

// Dang ky
const postSignUp = async (req, res) => {
  try {
    let { tai_khoan, ho_ten, email, so_dt, mat_khau, loai_nguoi_dung } =
      req.body;
    let data = {
      tai_khoan,
      ho_ten,
      email,
      so_dt,
      mat_khau,
      loai_nguoi_dung,
    };
    // check  email khong trung
    const checkUserEmail = await model.NguoiDung.findOne({
      where: { email },
    });
    const checkUserAccount = await model.NguoiDung.findOne({
      where: { tai_khoan },
    });
    if (checkUserEmail) {
      failCode(res, "", "Email đã tồn tại  ");
    } else if (checkUserAccount) {
      failCode(res, "", "Tài khoản đã tồn tại  ");
    } else {
      await model.NguoiDung.create(data);
      successCode(res, data, "Đăng ký thành công");
    }
  } catch (err) {
    errorCode(res, "Đăng ký thất bại ");
  }
};

// Dang nhap
const postLogin = async (req, res) => {
  try {
    let { tai_khoan, mat_khau } = req.body;
    const checkUser = await model.NguoiDung.findOne({ where: { tai_khoan } });
    if (checkUser.mat_khau === mat_khau) {
      let token = encodeToken(checkUser);
      successCode(res, { checkUser, token }, "dang nhap thanh cong");
    }
  } catch (err) {
    failCode(res, "", "Đăng nhập thất bại ");
  }
};

//Lay danh sach loai nguoi dung
const getListTypeOfUser = async (req, res) => {
  try {
    let data = [
      {
        maLoaiNguoiDung: "KhachHang",
        tenLoai: "Khách hàng",
      },
      {
        maLoaiNguoiDung: "QuanTri",
        tenLoai: "Quản trị",
      },
    ];
    successCode(res, data, "Xử lí thành công");
  } catch (err) {
    failCode(res, "", "Xử lí thất bại ");
  }
};

// Lay danh sach nguoi dung
const getListUser = async (req, res) => {
  try {
    let danhSachNguoiDung = await model.NguoiDung.findAll();
    successCode(res, danhSachNguoiDung, "Xử lí thành công");
  } catch {
    failCode(res, "", "Xử lí thất bại ");
  }
};

// Lay nguoi dung phan trang
const getUserListByPage = async (req, res) => {
  try {
    let { soTrang, soPhanTuTrenTrang } = req.query;
    let danhsachTrang = await model.NguoiDung.findAndCountAll({
      offset: +soTrang * +soPhanTuTrenTrang - +soPhanTuTrenTrang,
      limit: +soPhanTuTrenTrang,
    });
    let { count } = await model.NguoiDung.findAndCountAll();
    let content = {
      currentPage: soTrang,
      count: soPhanTuTrenTrang,
      totalPages: Math.ceil(count / soPhanTuTrenTrang),
      totalCount: count,
      items: danhsachTrang.rows,
    };

    successCode(res, content, "Xử lý thành công");
  } catch (err) {
    failCode(res, "", "Xử lí thất bại ");
  }
};

// Tim nguoi dung theo ten
const getFindUserByName = async (req, res) => {
  try {
    let { tuKhoa, soTrang, soPhanTuTrenTrang } = req.query;
    let timNguoiDung = await model.NguoiDung.findAll({
      where: {
        ho_ten: {
          [Op.like]: `%${tuKhoa}%`,
        },
      },
    });
    let begin = soTrang * soPhanTuTrenTrang - soPhanTuTrenTrang;
    let end = begin + soPhanTuTrenTrang;
    let tongSoTrang = Math.ceil(timNguoiDung.length / soPhanTuTrenTrang);
    let content = {
      totalPages: tongSoTrang,
      totalCount: timNguoiDung.length,
      currentPage: soTrang,
      count: soPhanTuTrenTrang,
      item: timNguoiDung.slice(begin, end),
    };
    successCode(res, content, "Xử lí thành công");
  } catch {
    failCode(res, "", "Xử lí thất bại ");
  }
};

// Them nguoi dung
const postNewUser = async (req, res) => {
  try {
    let { tai_khoan, ho_ten, email, so_dt, mat_khau, loai_nguoi_dung } =
      req.body;
    let userNew = {
      tai_khoan,
      ho_ten,
      email,
      so_dt,
      mat_khau,
      loai_nguoi_dung,
    };
    let result = await model.NguoiDung.create(userNew);
    successCode(res, result, "Thêm người dùng thành công");
  } catch (error) {
    failCode(res, "", "Xử lí thất bại ");
  }
};

// cap nhat thong tin nguoi dung
const putUpdateUser = async (req, res) => {
  try {
    let { tai_khoan, ho_ten, email, so_dt, mat_khau, loai_nguoi_dung } =
      req.body;
    // kiểm tra dữ liệu
    let checkUser = await model.NguoiDung.findOne({
      where: { tai_khoan: tai_khoan },
    });
    if (checkUser) {
      // cập nhật dữ liệu
      let userUpdate = {
        tai_khoan: tai_khoan,
        ho_ten,
        email,
        so_dt,
        mat_khau,
        loai_nguoi_dung,
      };
      await model.NguoiDung.update(userUpdate, {
        where: { tai_khoan: tai_khoan },
      });

      successCode(res, userUpdate, "Cập nhật thành công !");
    } else {
      failCode(res, "", "Tài khoản không tồn tại");
    }
  } catch (err) {
    errorCode(res, "Lỗi backend");
  }
};

// xoa nguoi dung
const deleteUser = async (req, res) => {
  try {
    let { tai_khoan } = req.params;

    // kiểm tra dữ liệu
    let checkUser = await model.NguoiDung.findOne({
      where: { tai_khoan: tai_khoan },
    });
    if (checkUser) {
      // cập nhật dữ liệu
      await model.NguoiDung.destroy({
        where: { tai_khoan: tai_khoan },
      });
      successCode(res, "", "Xoá người dùng thành công !");
    } else {
      failCode(res, "", "Tài khoản không tồn tại");
    }
  } catch (err) {
    errorCode(res, "Lỗi backend");
  }
};

module.exports = {
  postSignUp,
  postLogin,
  getListTypeOfUser,
  getListUser,
  getUserListByPage,
  getFindUserByName,
  postNewUser,
  putUpdateUser,
  deleteUser,
};
