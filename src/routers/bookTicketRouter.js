const express = require("express");
const bookTicketRouter = express.Router();
let {
  getTicketList,
  postBookTicket,
  postBookShedule,
} = require("../controllers/bookTicketController");
const { kiemTraToken } = require("../middleware/auth");

bookTicketRouter.get("/LayDanhSachPhongVe/:ma_lich_chieu", getTicketList);

bookTicketRouter.post("/DatVe", kiemTraToken, postBookTicket);

bookTicketRouter.post("/DatLichChieu", kiemTraToken, postBookShedule);

module.exports = bookTicketRouter;
