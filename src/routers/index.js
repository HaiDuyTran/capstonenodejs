const express = require("express");
const movieRouter = require("./movieRouter");
const userRouter = require("./userRouter");
const cinemaRouter = require("./cinemaRouter");
const bookTicketRouter = require("./bookTicketRouter");
const rootRouter = express.Router();

rootRouter.use("/QuanLyNguoiDung", userRouter);
rootRouter.use("/QuanLyPhim", movieRouter);
rootRouter.use("/QuanLyRap", cinemaRouter);
rootRouter.use("/QuanLyDatVe", bookTicketRouter);

module.exports = rootRouter;
