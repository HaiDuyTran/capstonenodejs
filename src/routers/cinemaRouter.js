const express = require("express");
const cinemaRouter = express.Router();

let {
  getCinemaSystemInfor,
  getCinemaClusterBySystem,
  getSheduleInforMovie,
  getSheduleInforSystem,
} = require("../controllers/cinemaController");

cinemaRouter.get("/LayThongTinHeThongRap", getCinemaSystemInfor);

cinemaRouter.get("/LayThongTinCumRapTheoHeThong", getCinemaClusterBySystem);

cinemaRouter.get("/LayThongTinLichChieuHeThongRap", getSheduleInforSystem);

cinemaRouter.get("/LayThongTinLichChieuPhim", getSheduleInforMovie);

module.exports = cinemaRouter;
