const express = require("express");
const userRouter = express.Router();
let { checkToken, kiemTraToken } = require("../middleware/auth");
let {
  postSignUp,
  postLogin,
  getListTypeOfUser,
  getListUser,
  getUserListByPage,
  getFindUserByName,
  postNewUser,
  putUpdateUser,
  deleteUser,
} = require("../controllers/userController");
// dang ky
userRouter.post("/DangKy", postSignUp);

// dang nhap
userRouter.post("/DangNhap", postLogin);

// lay danh sach loai nguoi dung
userRouter.get("/LayDanhSachLoaiNguoiDung", getListTypeOfUser);

// lay danh sach nguoi dung
userRouter.get("/LayDanhSachNguoiDung", getListUser);

// lay danh sach nguoi dung theo trang
userRouter.get("/LayDanhSachNguoiDungPhanTrang", getUserListByPage);

// tim nguoi dung theo ten
userRouter.get("/TimKiemNguoiDung", getFindUserByName);

// them nguoi dung
userRouter.post("/ThemNguoiDung", kiemTraToken, postNewUser);

// cap nhat thong tin nguoi dung
userRouter.put("/CapNhatThongTinNguoiDung", kiemTraToken, putUpdateUser);

// xoa nguoi dung
userRouter.delete("/XoaNguoiDung/:tai_khoan", kiemTraToken, deleteUser);

module.exports = userRouter;
