const express = require("express");
const movieRouter = express.Router();
const upload = require("../middleware/upload");

const {
  getBanner,
  getMovieList,
  getMovieListByPage,
  getMovieListByDate,
  getMovieDetail,
  postNewMovie,
  postUploadImage,
  putUpdateMovie,
  deleteMovie,
} = require("../controllers/movieController");
const { kiemTraToken } = require("../middleware/auth");

// GET
// lay danh sach banner
movieRouter.get("/LayDanhSachBanner", getBanner);

// lay danh sach phim
movieRouter.get("/LayDanhSachPhim", getMovieList);

// lay danh sach phim theo trang
movieRouter.get("/LayDanhSachPhimTheoTrang", getMovieListByPage);

// lay danh sach phim theo ngay
movieRouter.get("/LayDanhSachPhimTheoNgay", getMovieListByDate);

// lay thong tin phim theo ma phim
movieRouter.get("/LayThongTinPhim", getMovieDetail);

// POST

// upload hinh
movieRouter.post(
  "/ThemPhimUploadHinh",
  upload.single("upload"),
  postUploadImage
);

// tao phim moi
movieRouter.post("/", postNewMovie);

// cap nhat phim
movieRouter.put("/CapNhatPhimUpload/:id", kiemTraToken, putUpdateMovie);

// xoa phim
movieRouter.delete("/XoaPhim/:id", kiemTraToken, deleteMovie);

module.exports = movieRouter;
