const successCode = (res, data, message) => {
  res.status(200).json({
    statusCode: 200,
    message,
    content: data,
  });
};

const failCode = (res, data, message) => {
  res.status(400).json({
    statusCode: 400,
    message,
    content: data,
  });
};

const errorCode = (res, message) => {
  res.status(500).send(message);
};

module.exports = {
  successCode,
  failCode,
  errorCode,
};
