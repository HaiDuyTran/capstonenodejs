const express = require("express");
const app = express();
//
const cors = require("cors");
app.use(cors());
//middleware
app.use(express.json());
app.use(express.static("."));
app.listen(8080, () => {});

//localhost:8080/api/user/getUser
const rootRoute = require("./routers/index");
app.use("/api", rootRoute);
