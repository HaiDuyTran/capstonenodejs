/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `Banner` (
  `ma_banner` int NOT NULL AUTO_INCREMENT,
  `ma_phim` int DEFAULT NULL,
  `hinh_anh` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ma_banner`),
  KEY `ma_phim` (`ma_phim`),
  CONSTRAINT `Banner_ibfk_1` FOREIGN KEY (`ma_phim`) REFERENCES `Phim` (`ma_phim`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `CumRap` (
  `ma_cum_rap` varchar(255) NOT NULL,
  `ten_cum_rap` varchar(1024) DEFAULT NULL,
  `dia_chi` varchar(1024) DEFAULT NULL,
  `ma_he_thong_rap` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ma_cum_rap`),
  KEY `ma_he_thong_rap` (`ma_he_thong_rap`),
  CONSTRAINT `CumRap_ibfk_1` FOREIGN KEY (`ma_he_thong_rap`) REFERENCES `HeThongRap` (`ma_he_thong_rap`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `DatVe` (
  `tai_khoan` varchar(255) NOT NULL,
  `ma_lich_chieu` int NOT NULL,
  `ma_ghe` int DEFAULT NULL,
  PRIMARY KEY (`tai_khoan`,`ma_lich_chieu`),
  KEY `ma_lich_chieu` (`ma_lich_chieu`),
  KEY `ma_ghe` (`ma_ghe`),
  CONSTRAINT `DatVe_ibfk_1` FOREIGN KEY (`tai_khoan`) REFERENCES `NguoiDung` (`tai_khoan`),
  CONSTRAINT `DatVe_ibfk_2` FOREIGN KEY (`ma_lich_chieu`) REFERENCES `LichChieu` (`ma_lich_chieu`),
  CONSTRAINT `DatVe_ibfk_3` FOREIGN KEY (`ma_ghe`) REFERENCES `Ghe` (`ma_ghe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Ghe` (
  `ma_ghe` int NOT NULL AUTO_INCREMENT,
  `ten_ghe` varchar(255) DEFAULT NULL,
  `loai_ghe` varchar(255) DEFAULT NULL,
  `ma_rap` int DEFAULT NULL,
  PRIMARY KEY (`ma_ghe`),
  KEY `ma_rap` (`ma_rap`),
  CONSTRAINT `Ghe_ibfk_1` FOREIGN KEY (`ma_rap`) REFERENCES `RapPhim` (`ma_rap`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `HeThongRap` (
  `ma_he_thong_rap` varchar(255) NOT NULL,
  `ten_he_thong_rap` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ma_he_thong_rap`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `LichChieu` (
  `ma_lich_chieu` int NOT NULL AUTO_INCREMENT,
  `ma_rap` int NOT NULL,
  `ma_phim` int NOT NULL,
  `ngay_gio_chieu` datetime DEFAULT NULL,
  `gia_ve` int DEFAULT NULL,
  PRIMARY KEY (`ma_lich_chieu`,`ma_rap`,`ma_phim`),
  KEY `ma_rap` (`ma_rap`),
  KEY `ma_phim` (`ma_phim`),
  CONSTRAINT `LichChieu_ibfk_1` FOREIGN KEY (`ma_rap`) REFERENCES `RapPhim` (`ma_rap`),
  CONSTRAINT `LichChieu_ibfk_2` FOREIGN KEY (`ma_phim`) REFERENCES `Phim` (`ma_phim`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `NguoiDung` (
  `tai_khoan` varchar(255) NOT NULL,
  `ho_ten` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `so_dt` varchar(255) DEFAULT NULL,
  `mat_khau` varchar(255) DEFAULT NULL,
  `loai_nguoi_dung` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tai_khoan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Phim` (
  `ma_phim` int NOT NULL AUTO_INCREMENT,
  `ten_phim` varchar(255) DEFAULT NULL,
  `trailer` varchar(255) DEFAULT NULL,
  `hinh_anh` varchar(255) DEFAULT NULL,
  `mota` varchar(255) DEFAULT NULL,
  `ngay_khoi_chieu` date DEFAULT NULL,
  `danh_gia` int DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `dang_chieu` tinyint(1) DEFAULT NULL,
  `sap_chieu` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ma_phim`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `RapPhim` (
  `ma_rap` int NOT NULL AUTO_INCREMENT,
  `ten_rap` varchar(255) DEFAULT NULL,
  `ma_cum_rap` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ma_rap`),
  KEY `ma_cum_rap` (`ma_cum_rap`),
  CONSTRAINT `RapPhim_ibfk_1` FOREIGN KEY (`ma_cum_rap`) REFERENCES `CumRap` (`ma_cum_rap`)
) ENGINE=InnoDB AUTO_INCREMENT=751 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Banner` (`ma_banner`, `ma_phim`, `hinh_anh`) VALUES
(19, 12, 'https://movienew.cybersoft.edu.vn/hinhanh/ban-tay-diet-quy.png');
INSERT INTO `Banner` (`ma_banner`, `ma_phim`, `hinh_anh`) VALUES
(21, 15, 'https://movienew.cybersoft.edu.vn/hinhanh/lat-mat-48h.png');
INSERT INTO `Banner` (`ma_banner`, `ma_phim`, `hinh_anh`) VALUES
(22, 14, 'https://movienew.cybersoft.edu.vn/hinhanh/cuoc-chien-sinh-tu.png');

INSERT INTO `CumRap` (`ma_cum_rap`, `ten_cum_rap`, `dia_chi`, `ma_he_thong_rap`) VALUES
('bhd-star-cineplex-3-2', 'BHD Star Cineplex - 3/2', 'L5-Vincom 3/2, 3C Đường 3/2, Q.10', 'BHDStar');
INSERT INTO `CumRap` (`ma_cum_rap`, `ten_cum_rap`, `dia_chi`, `ma_he_thong_rap`) VALUES
('bhd-star-cineplex-bitexco', 'BHD Star Cineplex - Bitexco', 'L3-Bitexco Icon 68, 2 Hải Triều, Q.1', 'BHDStar');
INSERT INTO `CumRap` (`ma_cum_rap`, `ten_cum_rap`, `dia_chi`, `ma_he_thong_rap`) VALUES
('bhd-star-cineplex-pham-hung', 'BHD Star Cineplex - Phạm Hùng', 'L4-Satra Phạm Hùng, C6/27 Phạm Hùng, Bình Chánh', 'BHDStar');
INSERT INTO `CumRap` (`ma_cum_rap`, `ten_cum_rap`, `dia_chi`, `ma_he_thong_rap`) VALUES
('glx-huynh-tan-phat', 'GLX - Huỳnh Tấn Phát', '1362 Huỳnh Tấn Phát, KP1, Phú Mỹ, Q. 7', 'Galaxy'),
('glx-kinh-duong-vuong', 'GLX - Kinh Dương Vương', '718bis Kinh Dương Vương, Q.6', 'Galaxy'),
('glx-nguyen-du', 'GLX - Nguyễn Du', '116 Nguyễn Du, Q.1', 'Galaxy');

INSERT INTO `DatVe` (`tai_khoan`, `ma_lich_chieu`, `ma_ghe`) VALUES
('4Hoangvuaokok', 2, 2);
INSERT INTO `DatVe` (`tai_khoan`, `ma_lich_chieu`, `ma_ghe`) VALUES
('abc123', 3, 3);
INSERT INTO `DatVe` (`tai_khoan`, `ma_lich_chieu`, `ma_ghe`) VALUES
('admin005', 4, 4);
INSERT INTO `DatVe` (`tai_khoan`, `ma_lich_chieu`, `ma_ghe`) VALUES
('admintest', 5, 5),
('bin142', 6, 6),
('bob12345', 7, 7),
('checkadd12', 8, 8),
('duc3636', 9, 9),
('haiHai', 10, 10),
('HanChen1995', 1, 11),
('1gdsewrewss', 11, 12),
('haoadmin', 2, 12),
('hghgh123123', 3, 13),
('hiepadmin', 4, 14),
('hieuadmin', 5, 15),
('hohuynhdung', 6, 16),
('hophuc', 7, 17),
('hvta4', 8, 18),
('jack1994', 9, 19),
('khanh629', 10, 20);

INSERT INTO `Ghe` (`ma_ghe`, `ten_ghe`, `loai_ghe`, `ma_rap`) VALUES
(1, '01', 'Thuong', 451);
INSERT INTO `Ghe` (`ma_ghe`, `ten_ghe`, `loai_ghe`, `ma_rap`) VALUES
(2, '02', 'Thuong', 451);
INSERT INTO `Ghe` (`ma_ghe`, `ten_ghe`, `loai_ghe`, `ma_rap`) VALUES
(3, '03', 'Thuong', 451);
INSERT INTO `Ghe` (`ma_ghe`, `ten_ghe`, `loai_ghe`, `ma_rap`) VALUES
(4, '04', 'Thuong', 451),
(5, '05', 'Thuong', 451),
(6, '06', 'Thuong', 451),
(7, '07', 'Thuong', 451),
(8, '08', 'Thuong', 451),
(9, '09', 'Thuong', 451),
(10, '10', 'Thuong', 451),
(11, '11', 'Thuong', 451),
(12, '11', 'Thuong', 451),
(13, '12', 'Thuong', 451),
(14, '13', 'Thuong', 451),
(15, '14', 'Thuong', 451),
(16, '15', 'Thuong', 451),
(17, '16', 'Thuong', 451),
(18, '17', 'Thuong', 451),
(19, '18', 'Thuong', 451),
(20, '19', 'Thuong', 451),
(21, '20', 'Thuong', 451),
(22, '21', 'Thuong', 451),
(23, '22', 'Thuong', 451),
(24, '23', 'Thuong', 451),
(25, '24', 'Thuong', 451),
(26, '25', 'Thuong', 451),
(27, '26', 'Thuong', 451),
(28, '27', 'Thuong', 451),
(29, '28', 'Thuong', 451),
(30, '29', 'Thuong', 451),
(31, '30', 'Thuong', 451);

INSERT INTO `HeThongRap` (`ma_he_thong_rap`, `ten_he_thong_rap`, `logo`) VALUES
('BHDStar', 'BHD Star Cineplex', 'https://movienew.cybersoft.edu.vn/hinhanh/bhd-star-cineplex.png');
INSERT INTO `HeThongRap` (`ma_he_thong_rap`, `ten_he_thong_rap`, `logo`) VALUES
('CGV', 'cgv', 'https://movienew.cybersoft.edu.vn/hinhanh/cgv.png');
INSERT INTO `HeThongRap` (`ma_he_thong_rap`, `ten_he_thong_rap`, `logo`) VALUES
('CineStar', 'CineStar', 'https://movienew.cybersoft.edu.vn/hinhanh/cinestar.png');
INSERT INTO `HeThongRap` (`ma_he_thong_rap`, `ten_he_thong_rap`, `logo`) VALUES
('Galaxy', 'Galaxy Cinema', 'https://movienew.cybersoft.edu.vn/hinhanh/galaxy-cinema.png'),
('LotteCinima', 'Lotte Cinema', 'https://movienew.cybersoft.edu.vn/hinhanh/lotte-cinema.png'),
('MegaGS', 'MegaGS', 'https://movienew.cybersoft.edu.vn/hinhanh/megags.png');

INSERT INTO `LichChieu` (`ma_lich_chieu`, `ma_rap`, `ma_phim`, `ngay_gio_chieu`, `gia_ve`) VALUES
(1, 451, 1, '2019-01-01 00:00:00', 75000);
INSERT INTO `LichChieu` (`ma_lich_chieu`, `ma_rap`, `ma_phim`, `ngay_gio_chieu`, `gia_ve`) VALUES
(2, 452, 3, '2019-01-01 00:00:00', 75000);
INSERT INTO `LichChieu` (`ma_lich_chieu`, `ma_rap`, `ma_phim`, `ngay_gio_chieu`, `gia_ve`) VALUES
(3, 453, 4, '2019-01-01 00:00:00', 75000);
INSERT INTO `LichChieu` (`ma_lich_chieu`, `ma_rap`, `ma_phim`, `ngay_gio_chieu`, `gia_ve`) VALUES
(4, 454, 5, '2019-01-01 00:00:00', 75000),
(5, 455, 6, '2019-01-01 00:00:00', 75000),
(6, 456, 7, '2019-01-01 00:00:00', 75000),
(7, 457, 8, '2019-01-09 00:00:00', 75000),
(8, 458, 9, '2019-01-09 00:00:00', 75000),
(9, 459, 10, '2019-01-09 00:00:00', 75000),
(10, 460, 11, '2019-01-09 00:00:00', 75000),
(11, 741, 12, '2019-01-09 00:00:00', 75000),
(12, 742, 13, '2019-01-09 00:00:00', 75000),
(13, 743, 14, '2019-01-08 00:00:00', 75000),
(14, 744, 15, '2019-01-08 00:00:00', 75000),
(15, 745, 16, '2019-01-08 00:00:00', 75000),
(16, 746, 17, '2019-01-08 00:00:00', 75000),
(17, 747, 18, '2019-01-08 00:00:00', 75000),
(18, 748, 18, '2019-01-08 00:00:00', 75000),
(19, 749, 20, '2019-01-07 00:00:00', 75000),
(20, 750, 18, '2019-01-07 00:00:00', 75000),
(21, 455, 5, '2018-12-31 17:00:00', 32132130),
(22, 455, 5, '2018-12-31 17:00:00', 32132130),
(34, 455, 5, '2018-12-31 17:00:00', 32132130),
(35, 455, 5, '2018-12-31 17:00:00', 32132130),
(36, 455, 5, '2018-12-31 17:00:00', 32132130),
(37, 455, 5, '2018-12-31 17:00:00', 32132130),
(38, 455, 5, '2018-12-31 17:00:00', 32132130),
(39, 455, 5, '2018-12-31 17:00:00', 32132130);

INSERT INTO `NguoiDung` (`tai_khoan`, `ho_ten`, `email`, `so_dt`, `mat_khau`, `loai_nguoi_dung`) VALUES
('1123', 'heods', 'dasdasds@gmail.com', '0774651820', 'Tct2709123', 'QuanTri');
INSERT INTO `NguoiDung` (`tai_khoan`, `ho_ten`, `email`, `so_dt`, `mat_khau`, `loai_nguoi_dung`) VALUES
('1gdsewrewss', 'heods', 'admin13@gmail.com', '0774651820', 'Tct2709123', 'QuanTri');
INSERT INTO `NguoiDung` (`tai_khoan`, `ho_ten`, `email`, `so_dt`, `mat_khau`, `loai_nguoi_dung`) VALUES
('1gdsewrewss123', 'heods', 'admin12313@gmail.com', '0774651820', 'Tct2709123', 'QuanTri');
INSERT INTO `NguoiDung` (`tai_khoan`, `ho_ten`, `email`, `so_dt`, `mat_khau`, `loai_nguoi_dung`) VALUES
('1gdsewrewss1235436435', 'heods', 'adm12121in123131@gmail.com', '0774651820', 'Tct2709123', 'QuanTri'),
('1grwewrewss', 'heods', 'admintessvdzsdt123@gmail.com', '0774651820', 'Tct2709123', 'QuanTri'),
('250196', ' heo', 'dssaa@gmail.com', '0778046560', 'tcuahrTe344', 'QuanTri'),
('4Hoangvuaokok', 'phong0601', 'hoang_xam2@gmail.com', '0123456789999', 'HIHIMLEM', 'QuanTri'),
('abc123', 'Alice in America', 'eggg12379@gmail.com', '0356328888', '123456111', 'QuanTri'),
('abcd1123', 'hai', 'dasdasds1234@gmail.com', '0774651820', 'Tct2709123', 'QuanTri'),
('admin005', 'BC32E', 'BC32E@gmail.com', '123456780', 'admin0031', 'QuanTri'),
('admintest', 'Trần hoàng lâm', 'admintest123@gmail.com', '0974287784', 'admintest', 'QuanTri'),
('bin142', 'Bin', 'bin142@gmail.com', '1234567890', 'Bin142!!', 'QuanTri'),
('bob12345', 'Bob oc cho suc vat db rr', 'bobbob123456@gmail.com', '0903882123', 'Bob12345', 'KhachHang'),
('checkadd12', 'vu nguyen oc con lon', 'email124@gmail.com', '0391231232', '123456', 'KhachHang'),
('duc3636', 'Duc Cong', 'abc1235@gmail.com', '0916071072', 'nguyen1', 'KhachHang'),
('haiHai', 'bcsssss', 'testaasd@gmail.com', '0916071999', '01233210', 'KhachHang'),
('HanChen1995', 'Cau Be test', 'feasdve@gmail.com', '0356326859', 'e21e21e1', 'QuanTri'),
('haoadmin', 'Hào Nguyễn', 'babyloveyou@gmail.com', '0987654321', '123456789', 'QuanTri'),
('hghgh123123', 'Hoang Huu Nhanh', 'hoanghuunhanh.rain@gmail.com', '0326559894', '1231Ad34', 'QuanTri'),
('hiepadmin', 'No Error @', 'hoangnhathiep0@gmail.com', '0376249260', 'A44sdadas', 'QuanTri'),
('hieuadmin', 'Hoang Minh', 'khongco111@gmail.com', '012258456', '123456', 'QuanTri'),
('hoangntu56@gmail.com', 'hoang tu be nho123', 'testEdited1@gmail.com', '432432432423', '1234567', 'QuanTri'),
('hohuynhdung', 'awdwdwdw', 'hohuynhdung@cybersoft.vnnnnnnnnnnnnnnnnnnnnnnnnnnn', '0220342342341', '123456', 'QuanTri'),
('hophuc', 'hung213', '23456789@gmail.com', '0906677777', '1234567', 'Quantri'),
('hvta2', 'Đen Vâu qua', 'hvta2@gmail.com', '012345678984', NULL, 'KhachHang'),
('hvta4', 'hvta4', 'hvta4', 'hvta4', 'hvta4', 'KhachHang'),
('ihoib', 'heo', 'haidudfsy@gmail.com', '0774651820', 'Tct2709123', 'QuanTri'),
('ihoiob', 'heo', 'fd242564db@gmail.com', '0774651820', 'Tct2709123', 'QuanTri'),
('jack1994', 'nnn', 'jack1994@gmail.com', '07932299736', '123456', 'KhachHang'),
('khanh629', 'nguyễn duy khanh', 'color629@gmail.com', '0948844629', '123456789', 'QuanTri'),
('kimcuong', 'Kim cương vàng bạc đá quý', 'kimcuong502@gmail.com', '0987555552', 'kimcuong', 'QuanTri'),
('lananh1112', 'Lan Anh', 'lananh@gmail.com', '111111', '123456', 'KhachHang'),
('lt', 'lutrung', '123qbc@gmail.com', '0909090909', 'hungD@2k', 'QuanTri'),
('lygialam105', 'Lý Gia Lâm', 'lamlam@gmail.com', '0919113386', '24061985Tu', 'QuanTri'),
('maiquochuy', 'maiquochuy', 'huy@gmail.com', '9839189741', '123456', 'QuanTri'),
('manadmin', 'Man Ng', 'man@gmail.com', '12345678', '123123', 'QuanTri'),
('miku', 'Mikutest', 'miku@gmail.com', '09131231234', 'Miku@96', 'KhachHang'),
('minhtamtv123', 'nguyen la minh tam sn', 'tambin@gmail.com', '342998948', 'r4wrwer', 'KhachHang'),
('minion3012', 'Minion Moon', 'minion123@gmail.com', '0938123123', '123456', 'QuanTri'),
('nguyen121212', 'npn', '12912121212@gmail.com', '12312312312', '123456', 'KhachHang'),
('nguyenhuuhien713', 'Nguyễn Hiển', 'nguyenhuuhien@713gmail.com', '0123456789', 'Huuhien713@', 'QuanTri'),
('nhlong2711', 'Long Nguyen', 'longnguyen.030997@gmail.com', '0938012345', 'nhlong2711', 'KhachHang'),
('nhom789', 'nhom789', 'nhom789@gmail.com', '22222', '123456', 'QuanTri'),
('phat1', 'Nguyen Phat', 'phat01@gmail.com', '019585718', '123', 'QuanTri'),
('phuoc1', 'Phước', 'phuoc@gmail.com', '0999999999', '123456', 'KhachHang'),
('quocanh@admin', 'qdqwqqwwwqwdqdw', 'anhquochoadmin2000@gmai.com', '1231313', '42334234234', 'QuanTri'),
('qwe1', 'ewr', 'dfsdfe@gmai.com', '0908465248', 'Tdiw12345', 'KhachHang'),
('sexsygirl', 'meongoantest', 'pct.hcm111ut@gmail.com', '34343434343', '123', 'QuanTri'),
('sieunhangaodo', 'Hello Kitty', 'sieunhangao@gmail.com', '0234568987', 'sieunhangao', 'QuanTri'),
('sofm123', 'Hard', 'sofm@gmail.com', '5465465465', '123', 'KhachHang'),
('strinfdsg', 'strinfdsg', 'string@gmail.com', 'string', 'string', 'string'),
('string', 'strinfdsg', 'string@gmail.com', 'string1', 'string', 'string'),
('string1', 'string', 'string@yahoo.com', '09933333', 'string', 'QuanTri'),
('teishe13', 'Mess 24', 'messi@gmail.com', '099999321', 'Teishe123', 'KhachHang'),
('teishe20', 'teishe', 'email0022@gmail.com', '0999999998', 'Teishe123', 'KhachHang'),
('testne', 'Test Me', 'TestMe@gmail.com', '0991234567', 'tesstme', 'KhachHang'),
('userTest01', 'userTest01', 'userTest01@gmail.com', 'userTest01', '123456', 'QuanTri'),
('zasdasdasdasd23e23', '234234234', 'asdasdasdasds3333', 'asdasdasd', '234234234234', 'KhachHang');

INSERT INTO `Phim` (`ma_phim`, `ten_phim`, `trailer`, `hinh_anh`, `mota`, `ngay_khoi_chieu`, `danh_gia`, `hot`, `dang_chieu`, `sap_chieu`) VALUES
(1, 'Conan1234', 'https://www.youtube.com/watch?v=APwecL1rqwQ', 'https://movienew.cybersoft.edu.vn/hinhanh/conan-ban-tinh-ca-mau-do_gp01.jpeg', 'phim hayyy ', '2022-11-09', 7, 1, 1, 1);
INSERT INTO `Phim` (`ma_phim`, `ten_phim`, `trailer`, `hinh_anh`, `mota`, `ngay_khoi_chieu`, `danh_gia`, `hot`, `dang_chieu`, `sap_chieu`) VALUES
(3, 'Van Bai Sinh Tu 1', 'https://www.youtube.com/watch?v=MqCFXrd7Qk0', 'https://movienew.cybersoft.edu.vn/hinhanh/van-bai-sinh-tu_gp01.jpg', 'Phim hay', '2022-10-27', 6, 1, 1, 1);
INSERT INTO `Phim` (`ma_phim`, `ten_phim`, `trailer`, `hinh_anh`, `mota`, `ngay_khoi_chieu`, `danh_gia`, `hot`, `dang_chieu`, `sap_chieu`) VALUES
(4, '12 Angry Men (1957)', 'https://youtu.be/_13J_9B5jEk', 'https://movienew.cybersoft.edu.vn/hinhanh/12-angry-men-1957-_gp01.jpg', 'Phim hay', '2022-11-09', 9, 1, 1, 1);
INSERT INTO `Phim` (`ma_phim`, `ten_phim`, `trailer`, `hinh_anh`, `mota`, `ngay_khoi_chieu`, `danh_gia`, `hot`, `dang_chieu`, `sap_chieu`) VALUES
(5, 'Inception 2', 'https://youtu.be/YoHD9XEInc0', 'https://movienew.cybersoft.edu.vn/hinhanh/inception-2_gp01.jpg', 'Phim hay', '2022-10-17', 8, 1, 1, 1),
(6, 'Lion King – Vua sư tử (1994)', 'https://youtu.be/lFzVJEksoDY', 'https://movienew.cybersoft.edu.vn/hinhanh/lion-king-–-vua-su-tu-1994-_gp01.jpg', 'Phim hay', '2020-07-08', 10, 1, 1, 0),
(7, 'Terminator 2: Judgment Day', 'https://youtu.be/lwSysg9o7wE', 'https://movienew.cybersoft.edu.vn/hinhanh/terminator-2-judgment-day_gp01.jpg', 'Phim hay', '2022-10-11', 8, 1, 1, 0),
(8, 'Cat and Dog 2', 'Trailer', 'https://movienew.cybersoft.edu.vn/hinhanh/cat-and-dog-2_gp01.jpg', 'Phim hay', '2022-10-17', 6, 1, 1, 1),
(9, 'Giấc Mơ Của Mẹ', 'https://www.youtube.com/watch?v=la8uPwJVWjs', 'https://movienew.cybersoft.edu.vn/hinhanh/giac-mo-cua-me_gp01.jpg', 'Phim hay', '2022-10-18', 7, 1, 1, 0),
(10, 'Naruto 2', 'https://www.youtube.com/watch?v=yeUpnIKt6k4', 'https://movienew.cybersoft.edu.vn/hinhanh/naruto-2_gp01.jpg', 'Phim hay', '2022-11-04', 10, 0, 0, 1),
(11, 'AI MUỐN NGHE KHÔNG', 'https://www.youtube.com/watch?v=JxBnLmCOEJ8', 'https://movienew.cybersoft.edu.vn/hinhanh/ai-muon-nghe-khong_gp01.png', 'Phim hay', '2023-01-03', 5, 0, 0, 1),
(12, 'Kung Fu Panda12', 'https://www.youtube.com/watch?v=yCk9VAxEpD0', 'https://movienew.cybersoft.edu.vn/hinhanh/kung-fu-panda_gp01.jpg', 'Phim hay', '2022-11-01', 8, 0, 0, 1),
(13, 'Conan', 'https://youtu.be/n5rwEGgyh3A', 'https://movienew.cybersoft.edu.vn/hinhanh/conan_gp01.jpg', 'Phim hay', '2022-11-04', 10, 0, 1, 0),
(14, 'Avatar: The Way of Water', 'https://www.youtube.com/watch?v=d9MyW72ELq0', 'https://movienew.cybersoft.edu.vn/hinhanh/avatar-the-way-of-water_gp01.jpg', 'Phim hay', '2022-12-28', 9, 0, 0, 1),
(15, 'chiến binh báo đen', 'https://www.youtube.com/watch?v=APwecL1rqwQ', 'https://movienew.cybersoft.edu.vn/hinhanh/chien-binh-bao-den_gp01.jpg', 'Phim hay', '2022-11-03', 9, 1, 1, 1),
(16, 'Phim 1,Phim 1', 'Trailer phim 1', 'https://movienew.cybersoft.edu.vn/hinhanh/phim-1-phim-1_gp01.jpg', 'Phim hay', '2022-11-17', 8, 1, 1, 1),
(17, '111111111111', '11111111111', 'https://movienew.cybersoft.edu.vn/hinhanh/111111111111_gp01.png', 'Phim hay', '2022-11-28', 10, 1, 1, 1),
(18, 'asdasdas', 'dasdasdas', 'https://movienew.cybersoft.edu.vn/hinhanh/asdasdas_gp01.png', 'Phim hay', '2022-11-04', 10, 1, 1, 1),
(20, 'Conan: Bản Tình Ca Màu Đỏ ', 'https://www.youtube.com/watch?v=APwecL1rqwQ', 'https://movienew.cybersoft.edu.vn/hinhanh/conan-ban-tinh-ca-mau-do_gp01.jpeg', 'phim hay ', '2022-11-09', 7, 1, 1, 1),
(21, 'Conan: Bản Tình Ca Màu Đỏ ', 'https://www.youtube.com/watch?v=APwecL1rqwQ', 'https://movienew.cybersoft.edu.vn/hinhanh/conan-ban-tinh-ca-mau-do_gp01.jpeg', 'phim hay ', '2022-11-09', 7, 1, 1, 1);

INSERT INTO `RapPhim` (`ma_rap`, `ten_rap`, `ma_cum_rap`) VALUES
(451, 'Rạp 1', 'bhd-star-cineplex-3-2');
INSERT INTO `RapPhim` (`ma_rap`, `ten_rap`, `ma_cum_rap`) VALUES
(452, 'Rạp 2', 'bhd-star-cineplex-3-2');
INSERT INTO `RapPhim` (`ma_rap`, `ten_rap`, `ma_cum_rap`) VALUES
(453, 'Rạp 3', 'bhd-star-cineplex-3-2');
INSERT INTO `RapPhim` (`ma_rap`, `ten_rap`, `ma_cum_rap`) VALUES
(454, 'Rạp 4', 'bhd-star-cineplex-3-2'),
(455, 'Rạp 5', 'bhd-star-cineplex-3-2'),
(456, 'Rạp 6', 'bhd-star-cineplex-3-2'),
(457, 'Rạp 7', 'bhd-star-cineplex-3-2'),
(458, 'Rạp 8', 'bhd-star-cineplex-3-2'),
(459, 'Rạp 9', 'bhd-star-cineplex-3-2'),
(460, 'Rạp 10', 'bhd-star-cineplex-3-2'),
(741, 'Rạp 1', 'glx-huynh-tan-phat'),
(742, 'Rạp 2', 'glx-huynh-tan-phat'),
(743, 'Rạp 3', 'glx-huynh-tan-phat'),
(744, 'Rạp 4', 'glx-huynh-tan-phat'),
(745, 'Rạp 5', 'glx-huynh-tan-phat'),
(746, 'Rạp 6', 'glx-huynh-tan-phat'),
(747, 'Rạp 7', 'glx-huynh-tan-phat'),
(748, 'Rạp 8', 'glx-huynh-tan-phat'),
(749, 'Rạp 9', 'glx-huynh-tan-phat'),
(750, 'Rạp 10', 'glx-huynh-tan-phat');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;